<?php
session_start();
ini_set('display_errors', 0);
$groupId = filter_input(INPUT_GET, 'groupId');
header("Content-type:application/pdf");
header("Content-Disposition:attachment;filename=label" . date('_ymdhis') . ".pdf");
header('Cache-Control: public, must-revalidate, max-age=0');
header('Pragma: public');

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/deliveo.php';
require_once __DIR__ . '/lang/lang.php';
$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();
$deliveoApi = new Deliveo();

readfile($deliveoApi->labelUrl($groupId));
