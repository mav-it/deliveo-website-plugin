<?php
session_start();
$groupId = filter_input(INPUT_GET, 'groupId');
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/lang/lang.php';
require_once __DIR__ . '/deliveo.php';

$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();
$deliveoApi = new Deliveo();
$url = $deliveoApi->signatureUrl( $groupId );

if($url === false){
    die(Lang::instance()->_l('Nem tartozik aláíráslap a csomaghoz').'!');
}
header("Content-type:application/pdf");
header("Content-Disposition:attachment;filename=signature".date('_ymdhis').".pdf");
header('Cache-Control: public, must-revalidate, max-age=0');
header('Pragma: public');

readfile($url);