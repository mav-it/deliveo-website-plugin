# Deliveo website plugin

Weboldalba építhető Deliveo plugin. Képes feladni csomagot és lekérdezni már feladott csomagok státuszát. 

** Küldemény létrehozás **

Megadható a feladó, a címzett, és a számlafizető külön is, de a számlafizető megegyezhet a feladóval vagy a címzettel. Szállítási opcióként azok az opciók jelennek meg, amelyek a Deliveo példányban léteznek.

** Lekérdezés **

Írjuk be a DeliveoID-t a "DeliveoID" mezőbe, és az OK gomb megnyomásával lekérdezhetjük a csomag állapotát. (Ne felejtsük el beírni a Captchát.) Visszatérő adatként megkapjuk a feladó és a címzett adatait. Ha a keresés hozott találatot, akkor letölthetjük a csomaghoz kapcsolódó csomagcímkét is. Amennyiben a csomag már kézbesített (és be van kapcsolva a funkció) letölthetjük az aláíráslapot is, amin szerepel a kézbesítés helye és a címzett aláírása.

**Beállítások:**

A plugin sok beállítást tesz lehetővé a gyökérkönyvtárban elhelyezett .env fájlban. Első telepítéskor a gyökérkönyvtárban lévő .env.example file-ból készíts egy másolatot .env néven. A plugin frissítésekor a .env fájl nem íródik felül, a .env.example viszont igen. Erre az új funkciók miatt létrejövő új beállítási lehetőségek megjelenítése miatt van szükség. A plugin képes önmagát frissíteni a GitLab repóból, ha meghívod a /update URL-t a használati útvonal után. Pl.: ha a plugint a dwp.example.com URL-en használod, akkor a frissítést a dwp.example.com/update URL-en kell meghívnod. 

Demó: https://dwp.deliveo.eu/


# Deliveo website plugin

Deliveo plugin that can be built into a website. Able to drop a packet and query the status of packets already shipped.

** Create shipment **

The sender, recipient, and bill payer can be specified separately, but the bill payer can be the same as the sender or recipient. The options that exist in the Deliveo instance are displayed as delivery options.

** Query: **

Enter the DeliveoID in the "DeliveoID" field and press OK to query the status of the package. (Remember to enter the Captcha.) We return the sender and recipient information as return data. If the search returned a result, we can also download the package tag associated with the package. If the package has already been delivered (and the function is switched on), you can also download the signature sheet, which shows the place of delivery and the signature of the recipient.

** Settings: **

The plugin allows many settings in the .env file in the root directory. When you first install it, make a copy of the .env.example file in the root directory as .env. When the plugin is updated, the .env file is not overwritten, but the .env.example file is. This is necessary to show the new configuration options that are created because of the new features. The plugin can update itself from the GitLab repo by calling /update after the usage path. For example: if you are using the plugin at the dwp.example.com URL, you should call the update at the dwp.example.com/update URL.

Demo: https://dwp.deliveo.eu/
