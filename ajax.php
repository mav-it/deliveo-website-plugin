<?php
ini_set('display_errors', 0);
session_start();
require_once __DIR__ . '/lang/lang.php';
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/deliveo.php';

$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

// captcha validation
function captchaValidation()
{

    // customer_id session is set when new package created. 
    // This will disable to validate captcha after user created new package and redirected to getpackage-tab:
    if (isset($_SESSION['customer_id'])) {
        unset($_SESSION['customer_id']);
        return true;
    } else {
        return filter_var($_SESSION['captcha_code']) == filter_input(INPUT_POST, 'captcha', FILTER_DEFAULT);
    }
}

// Captcha ellenőrzés
function checkCaptcha()
{
    if (captchaValidation()) {
        echo json_encode(['success' => true]);
    } else {
        echo json_encode(['error' =>  Lang::instance()->_l('Érvénytelen captcha kód') . '!']);
    }
}

// Csomag feladás
function createPackage()
{
    $errors = [];
    $packages = [];
    $deliveoApi = new Deliveo();
    $requiredFields = [
        'sender',
        'sender_country',
        'sender_zip',
        'sender_city',
        'sender_address',
        'sender_phone',
        'consignee',
        'consignee_country',
        'consignee_zip',
        'consignee_city',
        'consignee_address',
        'consignee_phone',
        'colli',
        'currency',
    ];
    if (!captchaValidation()) {
        $errors['captcha'] = Lang::instance()->_l('Érvénytelen captcha kód') . '!';
    }

    $sendData = checkInputData();

    if (count($errors) === 0) {
        // küldés
        $response = $deliveoApi->packageCreate($sendData);
        echo json_encode($response);
    } else {
        echo json_encode(['errors' => $errors]);
    }
}


function editPackage()
{
    $errors = [];
    $packages = [];
    $deliveoApi = new Deliveo();
    $requiredFields = [
        'sender',
        'sender_country',
        'sender_zip',
        'sender_city',
        'sender_address',
        'sender_phone',
        'consignee',
        'consignee_country',
        'consignee_zip',
        'consignee_city',
        'consignee_address',
        'consignee_phone',
        'colli',
        'currency',
    ];
    if (!captchaValidation()) {
        $errors['captcha'] = Lang::instance()->_l('Érvénytelen captcha kód') . '!';
    }

    $sendData = checkInputData();

    if (count($errors) === 0) {
        // küldés
        $response = $deliveoApi->packageEdit($sendData);
        echo json_encode($response);
    } else {
        echo json_encode(['errors' => $errors]);
    }
}


function barionPayment()
{

    //csomag adatok session-be mentése
    $data = checkInputData();
    $_SESSION['input_data'] = json_encode($data);
    $result['error_code'] = 0;
    $result['type'] = 'success';

    if (filter_input(INPUT_POST, 'price_to_pay', FILTER_DEFAULT) > 0) {
        require_once 'plugins/barion-web-php/BarionClient.php';

        if (getenv('BARION_PROD_ENV') == 'true') {
            $BC = new BarionClient(getenv('BARION_POS_KEY'), 2, BarionEnvironment::Prod);
        } else {
            $BC = new BarionClient(getenv('BARION_POS_KEY'), 2, BarionEnvironment::Test);
        }

        // create the item model
        $item = new ItemModel();
        $item->Name = Lang::instance()->_l("Csomagfeladás"); // no more than 250 characters

        $description = filter_input(INPUT_POST, 'customer_name', FILTER_DEFAULT) . ': ' . filter_input(INPUT_POST, 'sender', FILTER_DEFAULT) . ' -> ' . filter_input(INPUT_POST, 'consignee', FILTER_DEFAULT);
        if (filter_input(INPUT_POST, 'tracking', FILTER_DEFAULT)) {
            $description .= ' (tracking: ' . filter_input(INPUT_POST, 'tracking', FILTER_DEFAULT) . ')';
        }

        $item->Description = $description; // no more than 500 characters
        $item->Quantity = 1;
        $item->Unit = Lang::instance()->_l("piece"); // no more than 50 characters
        $item->UnitPrice = filter_input(INPUT_POST, 'price_to_pay', FILTER_DEFAULT);
        $item->ItemTotal = filter_input(INPUT_POST, 'price_to_pay', FILTER_DEFAULT);
        $item->SKU = "ITEM-01"; // no more than 100 characters

        // create the transaction
        $trans = new PaymentTransactionModel();
        $trans->POSTransactionId = "TRANS-01";
        $trans->Payee = getenv('BARION_ACCOUNT_EMAIL'); // no more than 256 characters
        $trans->Total = filter_input(INPUT_POST, 'price_to_pay', FILTER_DEFAULT);
        $trans->Comment = $description; // no more than 640 characters
        $trans->AddItem($item); // add the item to the transaction

        // create the request model
        $psr = new PreparePaymentRequestModel();
        $psr->GuestCheckout = true; // we allow guest checkout
        $psr->PaymentType = PaymentType::Immediate; // we want an immediate payment
        $psr->FundingSources = array(FundingSourceType::All); // both Barion wallet and bank card accepted
        $psr->PaymentRequestId = "TESTPAY-01"; // no more than 100 characters
        $psr->PayerHint = filter_input(INPUT_POST, 'customer_email', FILTER_DEFAULT); // no more than 256 characters
        $lg = strtoupper($_SESSION['lang']);
        $psr->Locale = constant("UILocale::$lg");; // the UI language will be English 
        $currency = filter_input(INPUT_POST, 'currency', FILTER_DEFAULT);
        $psr->Currency = constant("Currency::$currency");
        $psr->RedirectUrl = getenv('BASE_URL') . '/ajax.php?action=check_payment';
        $psr->OrderNumber = "ORDER-0001"; // no more than 100 characters
        // $psr->ShippingAddress = $shippingAddress;
        $psr->AddTransaction($trans); // add the transaction to the payment

        // send the request
        $myPayment = $BC->PreparePayment($psr);

        if ($myPayment->RequestSuccessful === true) {
            // redirect the user to the Barion Smart Gateway
            if (getenv('BARION_PROD_ENV') == 'true') {
                $result['barion_url'] = "https://secure.barion.com/Pay?id=" . $myPayment->PaymentId;
            } else {
                $result['barion_url'] = "https://secure.test.barion.com/Pay?id=" . $myPayment->PaymentId;
            }
        } else {
            $result['type'] = 'error';
            $result['error_code'] = $myPayment->Errors[0]->ErrorCode;
            $result['error_msg'] = $myPayment->Errors[0]->Description;
        }
    } else {
        //Nem kell fizetni, csomag feladása
        $deliveoApi = new Deliveo();
        $response = $deliveoApi->packageCreate(json_decode($_SESSION['input_data']));
        $_SESSION['input_data'] = null;
        $result['barion_url'] = '/?tab=getpackage-tab&groupId=' . $response->data[0] . '&msg=' . $response->msg;
    }

    echo json_encode($result);
}


function checkInputData()
{

    $errors = ['result' => 'error'];
    $packages = [];
    $deliveoApi = new Deliveo();
    $requiredFields = [
        'sender',
        'sender_country',
        'sender_zip',
        'sender_city',
        'sender_address',
        'sender_phone',
        'consignee',
        'consignee_country',
        'consignee_zip',
        'consignee_city',
        'consignee_address',
        'consignee_phone',
        'colli',
        'currency',
    ];

    $packagesInputs = filter_input(INPUT_POST, 'packages', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

    $total_volume = 0;
    $total_weight = 0;

    if (is_null($packagesInputs)) {
        $errors[] = Lang::instance()->_l('Csomagok hozzáadása kötelező') . '!';
    } else {

        foreach ($packagesInputs as $package) {

            if (!$package['x']) {
                $package['x'] = 0;
            }
            if (!$package['y']) {
                $package['y'] = 0;
            }
            if (!$package['z']) {
                $package['z'] = 0;
            }

            $total_volume += ($package['x'] * $package['y'] * $package['z']);
            $total_weight += intval($package['weight']);

            foreach ($package as $index => $value) {
                if (array_key_exists($index, $requiredFields) && empty($value)) {
                    $errors[$index] = $requiredFields[$index] . ' ' . Lang::instance()->_l('mező kitöltése kötelező') . '!';
                }
            }
            $packages[] = $package;
        }
    }

    $customer_id = filter_input(INPUT_POST, 'customer_id', FILTER_DEFAULT);
    $group_id = filter_input(INPUT_POST, 'group_id', FILTER_DEFAULT);

    if ($customer_id) {
        $_SESSION["customer_id"] = $customer_id;
    } else {
        $customerData = [
            "customer_name" => filter_input(INPUT_POST, 'customer', FILTER_DEFAULT),
            "customer_zip" => filter_input(INPUT_POST, 'customer_zip', FILTER_DEFAULT),
            "customer_city" => filter_input(INPUT_POST, 'customer_city', FILTER_DEFAULT),
            "customer_address" => filter_input(INPUT_POST, 'customer_address', FILTER_DEFAULT),
            "customer_phone" => filter_input(INPUT_POST, 'customer_phone', FILTER_DEFAULT),
            "customer_email" => filter_input(INPUT_POST, 'customer_email', FILTER_DEFAULT),
            "customer_country" => filter_input(INPUT_POST, 'customer_country', FILTER_DEFAULT),
            "customer_building" => filter_input(INPUT_POST, 'customer_apartment', FILTER_DEFAULT)
        ];

        $customerCreateResponse = $deliveoApi->customerCreate($customerData);

        if ($customerCreateResponse->type === 'warning' || $customerCreateResponse->type === 'error') {
            $errors[] = $customerCreateResponse->msg;
        }
        $_SESSION["customer_id"] = $customerCreateResponse->customer_id;
    }

    $cod = filter_input(INPUT_POST, 'cod', FILTER_DEFAULT);
    $cod = (empty($cod)) ? '0' : $cod;

    $paid_amount = 0;
    if (getenv('BARION_PAYMENT')) {
        $paid_amount = filter_input(INPUT_POST, 'price_to_pay', FILTER_DEFAULT);
    }

    $sendData = [
        "group_id" => $group_id,
        "customer" => $_SESSION["customer_id"],
        "sender" => filter_input(INPUT_POST, 'sender', FILTER_DEFAULT),
        "sender_country" => filter_input(INPUT_POST, 'sender_country', FILTER_DEFAULT),
        "sender_zip" => filter_input(INPUT_POST, 'sender_zip', FILTER_DEFAULT),
        "sender_city" => filter_input(INPUT_POST, 'sender_city', FILTER_DEFAULT),
        "sender_address" => filter_input(INPUT_POST, 'sender_address', FILTER_DEFAULT),
        "sender_apartment" => filter_input(INPUT_POST, 'sender_apartment', FILTER_DEFAULT),
        "sender_phone" => filter_input(INPUT_POST, 'sender_phone', FILTER_DEFAULT),
        "sender_email" => filter_input(INPUT_POST, 'sender_email', FILTER_DEFAULT),
        "consignee" => filter_input(INPUT_POST, 'consignee', FILTER_DEFAULT),
        "consignee_country" => filter_input(INPUT_POST, 'consignee_country', FILTER_DEFAULT),
        "consignee_zip" => filter_input(INPUT_POST, 'consignee_zip', FILTER_DEFAULT),
        "consignee_city" => filter_input(INPUT_POST, 'consignee_city', FILTER_DEFAULT),
        "consignee_address" => filter_input(INPUT_POST, 'consignee_address', FILTER_DEFAULT),
        "consignee_apartment" => filter_input(INPUT_POST, 'consignee_apartment', FILTER_DEFAULT),
        "consignee_phone" => filter_input(INPUT_POST, 'consignee_phone', FILTER_DEFAULT),
        "consignee_email" => filter_input(INPUT_POST, 'consignee_email', FILTER_DEFAULT),
        "delivery" => filter_input(INPUT_POST, 'delivery', FILTER_DEFAULT),
        "optional_parameter_2" => filter_input(INPUT_POST, 'optional_parameter_2', FILTER_DEFAULT),
        "referenceid" => filter_input(INPUT_POST, 'referenceid', FILTER_DEFAULT),
        "cod" => $cod,
        "freight" => "felado",
        "comment" => filter_input(INPUT_POST, 'comment', FILTER_DEFAULT),
        "tracking" => filter_input(INPUT_POST, 'tracking', FILTER_DEFAULT),
        'packages' => $packages,
        'total_volume' => $total_volume,
        'total_weight' => $total_weight,
        'paid' => $paid_amount,
        'result' => "success",
        'pickup_location_id' => filter_input(INPUT_POST, 'pickup_location_id', FILTER_DEFAULT),
        'shop_id' => filter_input(INPUT_POST, 'shop_id', FILTER_DEFAULT)
    ];


    // kötelező mezők ellenőrzése
    foreach ($sendData as $index => $value) {
        if (array_key_exists($index, $requiredFields) && empty($value)) {
            $errors[$index] = $requiredFields[$index] . ' ' . Lang::instance()->_l('mező kitöltése kötelező') . '!';
        }
    }

    if (count($errors) > 1) {
        return $errors;
    } else {
        return $sendData;
    }
}


function checkPayment()
{
    require_once 'plugins/barion-web-php/BarionClient.php';

    $myPosKey = getenv('BARION_POS_KEY');
    $paymentId = $_GET['paymentId'];

    // Barion Client that connects to the TEST environment
    if (getenv('BARION_PROD_ENV') == 'true') {
        $BC = new BarionClient(getenv('BARION_POS_KEY'), 2, BarionEnvironment::Prod);
    } else {
        $BC = new BarionClient(getenv('BARION_POS_KEY'), 2, BarionEnvironment::Test);
    }

    // send the request
    $paymentDetails = $BC->GetPaymentState($paymentId);

    if ($paymentDetails->Status == "Succeeded") {
        //csomag feladása
        $deliveoApi = new Deliveo();
        $response = $deliveoApi->packageCreate(json_decode($_SESSION['input_data']));
        $_SESSION['input_data'] = null;
        header('Location: /?tab=getpackage-tab&groupId=' . $response->data[0] . '&msg=' . $response->msg);
    } else {
        //hibaüzenet kiírása
        $_SESSION["payment_error"] = implode(', ', $paymentDetails->Errors);
        header('Location: /?tab=send-tab');
    }
}


function getQuote()
{
    try {
        $client = new \GuzzleHttp\Client([
            // Base URI is used with relative requests
            'base_uri' => getenv('API_URL'),
            'headers' => [
                "Source" => "DWP",
            ]
        ]);

        $result = checkInputData();

        if ($result['result'] == "error") {
            echo json_encode(["type" => "error", "msg" => json_encode($result)]);
            return false;
        }
        $response = $client->request('POST', 'quote?licence=' . getenv('LICENCE') . '&api_key=' . getenv('API_KEY'), [
            'form_params' => $result
        ]);

        if ($response->getStatusCode() != 200) {
            $error = $response->getReasonPhrase();
            echo json_encode(["type" => "error", "msg" => $error]);
            return false;
        }

        echo (string) $response->getBody();
    } catch (Exception $e) {
        echo json_encode(["type" => "error", "msg" => $e->getMessage()]);
        return false;
    }
}


// csomag napló lekérdezése
function getPackage($captcha = true)
{
    $errors = [];
    if (!captchaValidation() && $captcha) {
        $errors['captcha'] = Lang::instance()->_l('Érvénytelen captcha kód') . '!';
        echo json_encode($errors);
    } else {
        $deliveoApi = new Deliveo();
        $groupId = filter_input(INPUT_POST, 'group_id', FILTER_DEFAULT);
        $packageDataResponse = $deliveoApi->getPackage($groupId);
        $packageLog = $deliveoApi->packageLog($groupId);

        $package_log_data_final = array();
        if (getenv('HIDE_PRICING_IN_THE_LOG') == "true") {
            foreach ($packageLog->data as $log) {
                if ($log->status != "pricing") {
                    $package_log_data_final[] = $log;
                }
            }
            $packageLog->data = $package_log_data_final;
        }

        $signatureUrl = $deliveoApi->signatureUrl($groupId);

        $response = [
            'getPackage' => $packageDataResponse,
            'packageLog' => $packageLog
        ];

        echo json_encode($response);
    }
}


function getCustomer()
{
    $deliveoApi = new Deliveo();
    $customer_id = filter_input(INPUT_POST, 'customer_id', FILTER_DEFAULT);

    $customerDataResponse = $deliveoApi->getCustomer($customer_id);

    $response = [
        'customer_data' => $customerDataResponse
    ];

    echo json_encode($response);
}


function deletePackage()
{
    $deliveoApi = new Deliveo();
    $group_id = filter_input(INPUT_POST, 'group_id', FILTER_DEFAULT);

    $response = $deliveoApi->deleteGroup($group_id);

    echo json_encode($response);
}


switch (filter_input(INPUT_GET, 'action', FILTER_DEFAULT)) {
    case "send":
        createPackage();
        break;
    case "edit":
        editPackage();
        break;
    case "barion_payment":
        barionPayment();
        break;
    case "check_payment":
        checkPayment();
        break;
    case "get":
        getPackage();
        break;
    case "getEditData":
        getPackage(false);
        break;
    case "deleteGroup":
        deletePackage();
        break;
    case "getCustomerData":
        getCustomer();
        break;
    case "checkcaptcha":
        checkCaptcha();
        break;
    case "getQuote":
        getQuote();
        break;
    default:
        echo "Unknown value for 'action'";
}
