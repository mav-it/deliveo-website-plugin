<?php

ini_set('display_errors', 1);

if (version_compare(PHP_VERSION, '5.5.0') < 0) {
	die(PHP_VERSION . ' >= 5.5 php version required.');
}
if (!extension_loaded('zlib')) {
	die('zlib php modul is required!');
}
session_start();
require_once __DIR__ . '/lang/lang.php';
// nyelv beállítása nyelvállasztás után
if (!is_null(filter_input(INPUT_GET, 'lang'))) {
	$_SESSION['lang'] = in_array(filter_input(INPUT_GET, 'lang'), Lang::instance()->getAcceptLang()) ? filter_input(INPUT_GET, 'lang') : 'en';
	header('Location: ' . $_SERVER['HTTP_REFERER'] ?? '/');
}

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/deliveo.php';
require_once __DIR__ . '/countries.php';

$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

// check env variable
if (empty(getenv('API_KEY'))) {
	die(Lang::instance()->_l('Be kell állítnai az API_KEY env változót') . '!');
}
if (empty(getenv('LICENCE'))) {
	die(Lang::instance()->_l('Be kell állítnai az LICENCE env változót') . '!');
}
if (empty(getenv('BASE_URL'))) {
	die(Lang::instance()->_l('Be kell állítnai az BASE_URL env változót') . '!');
}
if (!in_array('curl', get_loaded_extensions())) {
	die(Lang::instance()->_l('curl-t engedélyezni kell a szerveren') . '!');
}
if (getenv('BARION_PAYMENT') == '1' && (!getenv('BARION_POS_KEY') || !getenv('BARION_ACCOUNT_EMAIL'))) {
	die(Lang::instance()->_l('BARION_POS_KEY és BARION_ACCOUNT_EMAIL megadása kötelező') . '!');
}

$deliveoApi = new Deliveo();
$valid_key = $deliveoApi->validKey();
$default_currency = $valid_key['default_currency'];
$default_country = $valid_key['default_country'];

// API kulcs ellenőrzése
if (!$valid_key['is_valid']) {
	die(Lang::instance()->_l('Érvénytelen api kulcs') . '!');
}

$deliveryOptions = $deliveoApi->getDelivery();
$pickupPoints = $deliveoApi->getLocations(true);
$requiredFields = [
	'sender',
	'sender_country',
	'sender_zip',
	'sender_city',
	'sender_address',
	'sender_phone',
	'consignee',
	'consignee_country',
	'consignee_zip',
	'consignee_city',
	'consignee_address',
	'consignee_phone',
	'colli',
	'currency',
];

// aktuális tab beállítása
$tab = filter_input(INPUT_GET, 'tab');
if (is_null($tab)) {
	$tab = getenv('SEND_ON') ? 'send-tab' : 'getpackage-tab';
}

$groupId = filter_input(INPUT_GET, 'groupId');

if ($tab === 'getpackage-tab') {
	$message = filter_input(INPUT_GET, 'msg');
}

function url($url)
{
	return getenv('BASE_URL') . $url;
}
?>

<!DOCTYPE html>
<html lang="<?= $_SESSION['lang'] ?>">

<head>
	<meta charset="UTF-8">
	<title><?php echo getenv('APP_NAME'); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo url('/plugins/intl-tel-input-master/build/css/intlTelInput.css'); ?>">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&family=Open Sans&display=swap" rel="stylesheet">
	<?php if ($pickupPoints) { ?>
		<script>
			pickupPoints = <?php echo json_encode($pickupPoints); ?>;
			pickupPoints = Object.keys(pickupPoints).map(function(key) {
				return pickupPoints[key];
			});
		</script>

	<?php } ?>
	<style>
		.hide {
			display: none;
		}

		.service_price {
			display: inline-block;
			margin: 5px 0 0 20px;
		}

		.btn-success {
			float: left;
		}

		.barion_logo {
			text-align: center;
			padding: 20px 0 0 0;
		}

		.packages_summary {
			margin-bottom: 15px;
		}
	</style>
</head>

<body class="bg-light" style="font-family: 'Open Sans';">

	<?php
	if (isset($_SESSION['input_data'])) {
		$input_data = json_decode($_SESSION['input_data']);
	}
	if (isset($_SESSION['payment_error'])) {
	?>
		<script>
			alert(<?php echo $_SESSION['payment_error']; ?>);
		</script>
	<?php
		$_SESSION['payment_error'] = null;
	}
	?>

	<!-- nav -->

	<nav class="navbar navbar-expand-lg mx-auto" style="max-width: 1400px;">
		<div class="container-fluid">
			<a class="navbar-brand mr-0 mr-md-2 text-reset" href="<?php echo url(''); ?>" aria-label="Bootstrap">
				<img src="<?php echo url('/img/Deliveo_logo_64_trBG.png'); ?>" alt="">
				<?php echo getenv('APP_NAME'); ?>
			</a>
			<button class="navbar-toggler text-dark" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon">
					<i class="fas fa-bars"></i>
				</span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent" style="    flex-direction: row-reverse;">

				<ul class="nav nav-pills" style="justify-content: space-between;display: flex;">

					<?php if (getenv('SEND_ON')) { ?>
						<li class="nav-item">
							<a class="nav-link <?php echo ($tab === 'send-tab') ? 'active' : ''; ?>" href="<?php echo url('/?tab=send-tab'); ?>">
								<?php echo Lang::instance()->_l('Küldemény létrehozás'); ?>
							</a>
						</li>
					<?php } ?>
					<?php if (getenv('GET_PACKAGEDATA_ON')) { ?>
						<li class="nav-item">
							<a class="nav-link <?php echo ($tab === 'getpackage-tab') ? 'active' : ''; ?>" href="<?php echo url('/?tab=getpackage-tab'); ?>">
								<?php echo Lang::instance()->_l('Küldeményadatok lekérdezése'); ?>
							</a>
						</li>
					<?php } ?>
					<div class="dropdown">
						<button class="btn btn-sm bg-white  btn-icon" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<?php
							switch ($_SESSION['lang']) {
								case 'cs_CZ':
									$flag = 'cz';
									break;
								case 'el_GR':
									$flag = 'gr';
									break;
								case 'nb_NO':
									$flag = 'no';
									break;
								case 'en_GB':
									$flag = 'gb';
									break;
								case 'da_DK':
									$flag = 'dk';
									break;

								default:
									$flag = substr($_SESSION['lang'], 0, 2);
									break;
							}
							?>
							<img src="https://flagcdn.com/32x24/<?= $flag ?>.png" height="12" width="16">
						</button>
						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu2" style="column-count: 2;">
							<?php foreach (Lang::instance()->getAcceptLang() as $langItem) { ?>
								<a class="dropdown-item <?= $_SESSION['lang'] == $langItem ? "active" : "" ?> pl-2" href="<?php echo url('/?lang=' . $langItem); ?>">
									<?php
									switch ($langItem) {
										case 'cs_CZ':
											$flag = 'cz';
											break;
										case 'el_GR':
											$flag = 'gr';
											break;
										case 'nb_NO':
											$flag = 'no';
											break;
										case 'en_GB':
											$flag = 'gb';
											break;
										case 'da_DK':
											$flag = 'dk';
											break;

										default:
											$flag = substr($langItem, 0, 2);
											break;
									}
									?>
									<img class="img-fluid" src="https://flagcdn.com/32x24/<?= $flag ?>.png" height="12" width="16">

									<?php echo Lang::instance()->getlangText($langItem); ?>
								</a>
							<?php } ?>
						</div>
					</div>
			</div>



			</ul>
		</div>
		</div>
	</nav>
	<!-- nav vége -->
	<div class="container-fluid mt-2" style="max-width: 1400px;">
		<!-- tabok -->
		<div class="tab-content" id="pills-tabContent">

			<!-- csomag feladás tab -->
			<?php if ($tab === 'send-tab' && getenv('SEND_ON')) {
			?>
				<div id="pills-send">

					<div class="tab-pane active" id="send-tab" role="tabpanel" aria-labelledby="send-tab">
						<div class="card">
							<h1 class="card-header bg-primary text-white">
								<?php echo Lang::instance()->_l('Küldemény létrehozás') ?>
							</h1>
							<div class="card-body">
								<div class="alert alert-success hide">
								</div>
								<div class="row">
									<div class="col-lg-8">
										<?php
										$form_action = url('/ajax.php?action=send');
										if (getenv('BARION_PAYMENT')) {
											$form_action = url('/ajax.php?action=barion_payment');
										}
										?>
										<form method="POST" action="<?php echo $form_action; ?>" id="send-form">

											<!-- Feladó -->
											<h2><?php echo Lang::instance()->_l('Feladó'); ?></h2>

											<div class="form-group">
												<label for="sender">
													<?php echo Lang::instance()->_l('Feladó'); ?>
													<?php echo Lang::instance()->_l('név'); ?>
													<?php echo (in_array('sender', $requiredFields) ? '*' : ''); ?></label>
												<input type="text" name="sender" class="form-control" maxlength="25" <?php echo (in_array('sender', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->sender)) {
																																																echo 'value="' . $input_data->sender . '"';
																																															} ?>>
											</div>

											<div class="form-row">
												<div class="form-group col-lg-4">
													<label for="sender_country">
														<?php echo Lang::instance()->_l('Ország'); ?>
														<?php echo (in_array('sender_country', $requiredFields) ? '*' : ''); ?></label>
													<select name="sender_country" class="form-control" <?php echo (in_array('sender_country', $requiredFields) ? 'required' : ''); ?>>

														<?php foreach ($countries as $value => $name) { ?>
															<option <?php if (isset($input_data->sender_country) && $input_data->sender_country == $value) {
																		echo 'selected';
																	} ?> value="<?php echo $value; ?>"><?php echo $name; ?></option>
														<?php } ?>

													</select>
												</div>
												<div class="form-group col-lg-2">
													<label for="sender_zip">
														<?php echo Lang::instance()->_l('Irányítószám'); ?>
														<?php echo (in_array('sender_zip', $requiredFields) ? '*' : ''); ?></label>
													<input type="text" name="sender_zip" class="form-control" maxlength="15" <?php echo (in_array('sender_zip', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->sender_zip)) {
																																																			echo 'value="' . $input_data->sender_zip . '"';
																																																		} ?>>
												</div>
												<div class="form-group col-lg-6">
													<label for="sender_city">
														<?php echo Lang::instance()->_l('Település'); ?>
														<?php echo (in_array('sender_city', $requiredFields) ? '*' : ''); ?></label>
													<input type="text" name="sender_city" class="form-control" maxlength="50" <?php echo (in_array('sender_city', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->sender_city)) {
																																																				echo 'value="' . $input_data->sender_city . '"';
																																																			} ?>>
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-lg-6">
													<label for="sender_address">
														<?php echo Lang::instance()->_l('Utca, házszám'); ?>
														<?php echo (in_array('sender_address', $requiredFields) ? '*' : ''); ?></label>
													<input type="text" name="sender_address" class="form-control" maxlength="50" <?php echo (in_array('sender_address', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->sender_address)) {
																																																					echo 'value="' . $input_data->sender_address . '"';
																																																				} ?>>
												</div>
												<div class="form-group col-lg-6">
													<label for="sender_apartment">
														<?php echo Lang::instance()->_l('Épület, emelet, ajtó'); ?>
														<?php echo (in_array('sender_apartment', $requiredFields) ? '*' : ''); ?></label>
													<input type="text" name="sender_apartment" class="form-control" maxlength="50" <?php echo (in_array('sender_apartment', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->sender_apartment)) {
																																																						echo 'value="' . $input_data->sender_apartment . '"';
																																																					} ?>>
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-lg-6" id="sender_phone">
													<label for="sender_phone">
														<?php echo Lang::instance()->_l('Telefonszám'); ?>
														<?php echo (in_array('sender_phone', $requiredFields) ? '*' : ''); ?></label>
													<input title="eg.: +36201234567" placeholder="+3611234567" type="tel" name="sender_phone" class="form-control" maxlength="25" <?php echo (in_array('sender_phone', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->sender_phone)) {
																																																																	echo 'value="' . $input_data->sender_phone . '"';
																																																																} ?>>

													<div class="hide text-success valid-msg">✓ Valid</div>
													<div class="hide text-danger error-msg"></div>
												</div>
												<div class="form-group col-lg-6">
													<label for="sender_email">Email
														<?php echo (in_array('sender_email', $requiredFields) ? '*' : ''); ?></label>
													<input type="email" name="sender_email" class="form-control" maxlength="50" <?php echo (in_array('sender_email', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->sender_email)) {
																																																					echo 'value="' . $input_data->sender_email . '"';
																																																				} ?>>
												</div>
											</div>
											<?php if ($pickupPoints) { ?>
												<div class="form-row">
													<div class="form-group col-lg-6">
														<label for="pickup_location_id">
															<?php echo Lang::instance()->_l('Csomagpont'); ?>
														</label>
														<select name="pickup_location_id" id="pickup_location_id" class="form-control custom-select">
															<option value="" selected><?php echo Lang::instance()->_l('Cím szerint'); ?></option>
															<?php foreach ($pickupPoints as $pickupPoint) { ?>
																<option value="<?php echo $pickupPoint['location_id']; ?>"><?php echo $pickupPoint['name']; ?></option>
															<?php } ?>

														</select>
													</div>
												</div>
											<? } ?>
											<!-- Feladó -->

											<hr>
											<!-- Címzett -->
											<h2><?php echo Lang::instance()->_l('Címzett'); ?></h2>
											<div class="form-group">
												<label for="consignee">
													<?php echo Lang::instance()->_l('Címzett'); ?>
													<?php echo Lang::instance()->_l('név'); ?>
													<?php echo (in_array('consignee', $requiredFields) ? '*' : ''); ?></label>
												<input type="text" name="consignee" class="form-control" maxlength="25" <?php echo (in_array('consignee', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->consignee)) {
																																																		echo 'value="' . $input_data->consignee . '"';
																																																	} ?>>
											</div>
											<div class="form-row">
												<div class="form-group col-lg-4">
													<label for="consignee_country">
														<?php echo Lang::instance()->_l('Ország'); ?>
														<?php echo (in_array('consignee_country', $requiredFields) ? '*' : ''); ?></label>
													<select name="consignee_country" class="form-control" <?php echo (in_array('consignee_country', $requiredFields) ? 'required' : ''); ?>>
														<?php foreach ($countries as $value => $name) { ?>
															<option <?php if (isset($input_data->consignee_country) && $input_data->consignee_country == $value) {
																		echo 'selected';
																	} ?> value="<?php echo $value; ?>"><?php echo $name; ?></option>
														<?php } ?>
													</select>
												</div>
												<div class="form-group col-lg-2">
													<label for="consignee_zip">
														<?php echo Lang::instance()->_l('Irányítószám'); ?>
														<?php echo (in_array('consignee_zip', $requiredFields) ? '*' : ''); ?></label>
													<input type="text" name="consignee_zip" class="form-control" maxlength="15" <?php echo (in_array('consignee_zip', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->consignee_zip)) {
																																																					echo 'value="' . $input_data->consignee_zip . '"';
																																																				} ?>>
												</div>

												<div class="form-group col-lg-6">
													<label for="consignee_city">
														<?php echo Lang::instance()->_l('Település'); ?>
														<?php echo (in_array('consignee_city', $requiredFields) ? '*' : ''); ?></label>
													<input type="text" name="consignee_city" class="form-control" maxlength="50" <?php echo (in_array('consignee_city', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->consignee_city)) {
																																																					echo 'value="' . $input_data->consignee_city . '"';
																																																				} ?>>
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-lg-6">
													<label for="consignee_address">
														<?php echo Lang::instance()->_l('Utca, házszám'); ?>
														<?php echo (in_array('sender', $requiredFields) ? '*' : ''); ?></label>
													<input type="text" name="consignee_address" class="form-control" maxlength="50" <?php echo (in_array('consignee_address', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->consignee_address)) {
																																																							echo 'value="' . $input_data->consignee_address . '"';
																																																						} ?>>
												</div>
												<div class="form-group col-lg-6">
													<label for="consignee_apartment">
														<?php echo Lang::instance()->_l('Épület, emelet, ajtó'); ?>
														<?php echo (in_array('consignee_apartment', $requiredFields) ? '*' : ''); ?></label>
													<input type="text" name="consignee_apartment" class="form-control" maxlength="50" <?php echo (in_array('consignee_apartment', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->consignee_apartment)) {
																																																								echo 'value="' . $input_data->consignee_apartment . '"';
																																																							} ?>>
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-lg-6" id="consignee_phone">
													<label for="consignee_phone">
														<?php echo Lang::instance()->_l('Telefonszám'); ?>
														<?php echo (in_array('consignee_phone', $requiredFields) ? '*' : ''); ?></label>
													<input title="eg.: +36201234567" placeholder="+3611234567" type="tel" name="consignee_phone" class="form-control" maxlength="25" <?php echo (in_array('consignee_phone', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->consignee_phone)) {
																																																																			echo 'value="' . $input_data->consignee_phone . '"';
																																																																		} ?>>
													<div class="hide text-success valid-msg">✓ Valid</div>
													<div class="hide text-danger error-msg"></div>
												</div>
												<div class="form-group col-lg-6">
													<label for="consignee_email">Email
														<?php echo (in_array('consignee_email', $requiredFields) ? '*' : ''); ?></label>
													<input type="email" name="consignee_email" class="form-control" maxlength="50" <?php echo (in_array('consignee_email', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->consignee_email)) {
																																																						echo 'value="' . $input_data->consignee_email . '"';
																																																					} ?>>
												</div>
											</div>
											<?php if ($pickupPoints) { ?>
												<div class="form-row">
													<div class="form-group col-lg-6">
														<label for="shop_id">
															<?php echo Lang::instance()->_l('Csomagpont'); ?>
														</label>
														<select name="shop_id" id="shop_id" class="form-control custom-select">
															<option value="" selected><?php echo Lang::instance()->_l('Cím szerint'); ?></option>
															<?php foreach ($pickupPoints as $pickupPoint) { ?>
																<option value="<?php echo $pickupPoint['location_id']; ?>"><?php echo $pickupPoint['name']; ?></option>
															<?php } ?>

														</select>
													</div>
												</div>
											<? } ?>
											<!-- Címzett vége -->
											<hr>

											<div class="customer_container">
												<!-- Számlafizető -->
												<h2><?php echo Lang::instance()->_l('Számlafizető'); ?></h2>
												<div class="form-group">
													<input type="radio" name="customer_data" value="1" checked=""> <?php echo Lang::instance()->_l('Megegyezik a feladóval'); ?><br>
													<input type="radio" name="customer_data" value="2"> <?php echo Lang::instance()->_l('Megegyezik a címzettel'); ?><br>
													<input type="radio" name="customer_data" value="0"> <?php echo Lang::instance()->_l('Megadom az adatokat'); ?>
												</div>
												<div class="form-group">
													<label for="customer">
														<?php echo Lang::instance()->_l('Számlafizető'); ?>
														<?php echo Lang::instance()->_l('név'); ?>
														<?php echo (in_array('customer', $requiredFields) ? '*' : ''); ?></label>
													<input type="text" name="customer" class="form-control" maxlength="25" <?php echo (in_array('customer', $requiredFields) ? 'required' : ''); ?>>
												</div>
												<div class="form-row">
													<div class="form-group col-lg-4">
														<label for="customer_country">
															<?php echo Lang::instance()->_l('Ország'); ?>
															<?php echo (in_array('customer_country', $requiredFields) ? '*' : ''); ?></label>
														<select name="customer_country" class="form-control" <?php echo (in_array('customer_country', $requiredFields) ? 'required' : ''); ?>>
															<?php foreach ($countries as $value => $name) { ?>
																<option value="<?php echo $value; ?>"><?php echo $name; ?></option>
															<?php } ?>
														</select>
													</div>
													<div class="form-group col-lg-2">
														<label for="customer_zip">
															<?php echo Lang::instance()->_l('Irányítószám'); ?>
															<?php echo (in_array('customer_zip', $requiredFields) ? '*' : ''); ?></label>
														<input type="text" name="customer_zip" class="form-control" maxlength="15" <?php echo (in_array('customer_zip', $requiredFields) ? 'required' : ''); ?>>
													</div>

													<div class="form-group col-lg-6">
														<label for="customer_city">
															<?php echo Lang::instance()->_l('Település'); ?>
															<?php echo (in_array('customer_city', $requiredFields) ? '*' : ''); ?></label>
														<input type="text" name="customer_city" class="form-control" maxlength="50" <?php echo (in_array('customer_city', $requiredFields) ? 'required' : ''); ?>>
													</div>
												</div>
												<div class="form-row">
													<div class="form-group col-lg-6">
														<label for="customer_address">
															<?php echo Lang::instance()->_l('Utca, házszám'); ?>
															<?php echo (in_array('customer_address', $requiredFields) ? '*' : ''); ?></label>
														<input type="text" name="customer_address" class="form-control" maxlength="50" <?php echo (in_array('customer_address', $requiredFields) ? 'required' : ''); ?>>
													</div>
													<div class="form-group col-lg-6">
														<label for="customer_apartment">
															<?php echo Lang::instance()->_l('Épület, emelet, ajtó'); ?>
															<?php echo (in_array('customer_apartment', $requiredFields) ? '*' : ''); ?></label>
														<input type="text" name="customer_apartment" class="form-control" maxlength="50" <?php echo (in_array('customer_apartment', $requiredFields) ? 'required' : ''); ?>>
													</div>
												</div>
												<div class="form-row">
													<div class="form-group col-lg-6" id="customer_phone">
														<label for="customer_phone">
															<?php echo Lang::instance()->_l('Telefonszám'); ?>
															<?php echo (in_array('customer_phone', $requiredFields) ? '*' : ''); ?></label>
														<input title="eg.: +36201234567" placeholder="+3611234567" type="tel" name="customer_phone" class="form-control" maxlength="25" <?php echo (in_array('customer_phone', $requiredFields) ? 'required' : ''); ?>>
														<div class="hide text-success valid-msg">✓ Valid</div>
														<div class="hide text-danger error-msg"></div>
													</div>
													<div class="form-group col-lg-6">
														<label for="customer_email">Email<?php echo (in_array('customer_email', $requiredFields) ? '*' : ''); ?></label>
														<input type="email" name="customer_email" class="form-control" maxlength="50" <?php echo (in_array('customer_email', $requiredFields) ? 'required' : ''); ?>>
													</div>
												</div>

												<!-- Számlafizető vége -->
												<hr>
											</div>
											<input type="hidden" name="customer_id" value="">

											<!-- További adatok -->
											<h2><?php echo Lang::instance()->_l('További adatok'); ?></h2>
											<div class="form-row">
												<div class="col-lg-12">
													<div class="form-group">
														<label for="delivery">
															<?php echo Lang::instance()->_l('Szállítási opció'); ?>
															<?php echo (in_array('delivery', $requiredFields) ? '*' : ''); ?></label>
														<select name="delivery" class="form-control" <?php echo (in_array('delivery', $requiredFields) ? 'required' : ''); ?>>
															<?php foreach ($deliveryOptions as $value => $name) { ?>
																<option <?php if (isset($input_data->delivery) && $input_data->delivery == $value) {
																			echo 'selected';
																		} ?> value="<?php echo $value; ?>"><?php echo $name; ?></option>
															<?php } ?>
														</select>
													</div>


													<div class="form-row">
														<div class="form-group col-lg-6">
															<label for="referenceid">
																<?php echo Lang::instance()->_l('Hivatkozási szám okmánykezelés esetén'); ?>
																<?php echo (in_array('referenceid', $requiredFields) ? '*' : ''); ?></label>
															<input type="number" step="any" name="referenceid" class="form-control" maxlength="25" <?php echo (in_array('referenceid', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->referenceid)) {
																																																									echo 'value="' . $input_data->referenceid . '"';
																																																								} ?>>
														</div>
														<div class="form-group col-lg-6">
															<label for="tracking">
																<?php echo Lang::instance()->_l('Követőkód'); ?>
																<?php echo (in_array('tracking', $requiredFields) ? '*' : ''); ?></label>
															<input type="text" name="tracking" class="form-control" maxlength="25" <?php echo (in_array('tracking', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->tracking)) {
																																																				echo 'value="' . $input_data->tracking . '"';
																																																			} ?>>
														</div>
													</div>

													<div class="form-row">
														<div class="form-group col-lg-6">
															<label for="cod">
																<?php echo Lang::instance()->_l('Utánvét összege'); ?></label>

															<div class="input-group mb-3">
																<input type="text" name="cod" class="form-control" <?php echo (in_array('cod', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->cod)) {
																																															echo 'value="' . $input_data->cod . '"';
																																														} else {
																																															echo "value='0'";
																																														} ?>>
																<span class="input-group-text" id="cod"><?php echo $default_currency; ?></span>
															</div>
														</div>
														<div class="form-group col-lg-6">
															<label for="optional_parameter_2">
																<?php echo Lang::instance()->_l('Opcionális paraméter #2'); ?>
																<?php echo (in_array('optional_parameter_2', $requiredFields) ? '*' : ''); ?></label>
															<input type="text" name="optional_parameter_2" class="form-control" <?php echo (in_array('optional_parameter_2', $requiredFields) ? 'required' : ''); ?> <?php if (isset($input_data->optional_parameter_2)) {
																																																							echo 'value="' . $input_data->optional_parameter_2 . '"';
																																																						} ?>>
														</div>
													</div>

												</div>


											</div>
											<div class="form-row">
												<div class="col-lg-6">

													<div class="form-group form-check ">
														<input <?php if (isset($input_data->optional_parameter_1)) {
																	echo 'checked';
																} ?> name="optional_parameter_1" type="checkbox" value="1" class="form-check-input" id="optional_parameter_1">
														<label class="form-check-label" for="optional_parameter_1">
															<?php echo Lang::instance()->_l('Opcionális paraméter #1'); ?> </label>
													</div>
												</div>
												<div class="col-lg-6">

													<div class="form-group form-check">
														<input name="optional_parameter_3" <?php if (isset($input_data->optional_parameter_3)) {
																								echo 'checked';
																							} ?> type="checkbox" value="1" class="form-check-input" id="optional_parameter_3">
														<label class="form-check-label" for="optional_parameter_3">
															<?php echo Lang::instance()->_l('Opcionális paraméter #3'); ?> </label>
													</div>
												</div>
											</div>
											<div class="form-row">
												<div class="col-lg-12">
													<div class="form-group">
														<label for="comment">
															<?php echo Lang::instance()->_l('Megjegyzés'); ?>
															<?php echo (in_array('comment', $requiredFields) ? '*' : ''); ?></label>
														<textarea name="comment" class="form-control" maxlength="255" rows="2"></textarea>
													</div>
												</div>
											</div>
											<!-- További adatok vége -->

											<!-- Csomagok -->
											<h4><?php echo Lang::instance()->_l('Csomagok'); ?></h4>

											<div class="table-responsive">
												<table class="table table-bordered table-sm" id="packages">
													<thead>
														<tr>
															<th scope="col"><?php echo Lang::instance()->_l('Súly (kg)'); ?> <?php echo (in_array('weight', $requiredFields) ? '*' : ''); ?></th>
															<th scope="col">X (cm)<?php echo (in_array('x', $requiredFields) ? '*' : ''); ?></th>
															<th scope="col">Y (cm)<?php echo (in_array('y', $requiredFields) ? '*' : ''); ?></th>
															<th scope="col">Z (cm)<?php echo (in_array('z', $requiredFields) ? '*' : ''); ?></th>
															<th scope="col"><?php echo Lang::instance()->_l('Egyedi azonosító'); ?> <?php echo (in_array('customcode', $requiredFields) ? '*' : ''); ?></th>
															<th scope="col"></th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td class="form-group"><input type="number" min="1" class="packages_weight form-control" name="packages[0][weight]" <?php echo (in_array('weight', $requiredFields) ? 'required' : ''); ?>></td>
															<td class="form-group"><input type="number" min="1" class="packages_x form-control" name="packages[0][x]" <?php echo (in_array('x', $requiredFields) ? 'required' : ''); ?>></td>
															<td class="form-group"><input type="number" min="1" class="packages_y form-control" name="packages[0][y]" <?php echo (in_array('y', $requiredFields) ? 'required' : ''); ?>></td>
															<td class="form-group"><input type="number" min="1" class="packages_z form-control" name="packages[0][z]" <?php echo (in_array('z', $requiredFields) ? 'required' : ''); ?>></td>
															<td class="form-group"><input type="text" class="form-control packages_custom_code" name="packages[0][customcode]" maxlength="30" /><input type="hidden" class="form-control packages_package_id" name="packages[0][package_id]" /></td>
															<td><button class="btn btn-warning remove-item" type="button">x</button></td>
														</tr>
													</tbody>
												</table>
											</div>

											<div class="packages_summary">
												<?php echo Lang::instance()->_l('Összsúly'); ?>: <span class="weight_summary">-</span> kg, <?php echo Lang::instance()->_l('össztérfogat'); ?>: <span class="volume_summary">-</span> cm3, <?php echo Lang::instance()->_l('térfogatsúly'); ?>: <span class="dim_summary">-</span>
											</div>

											<button type="button" class="btn btn-success" id="add-package-row"> + <?php echo Lang::instance()->_l('Csomag hozzáadása'); ?></button>

											<div class="service_price">
												<h4></h4>
												<input type="hidden" name="price_to_pay" class="price_to_pay" value="0">
												<input type="hidden" name="currency" class="currency" value="HUF">
											</div>
											<hr>
											<!-- Csomagok vége -->
											<?php if (getenv("TERMS")) { ?>
												<div class="form-group form-check">
													<input name="terms" type="checkbox" class="form-check-input" id="terms" required>
													<label class="form-check-label" for="terms"><?php echo Lang::instance()->_l('Kijelentem, hogy elolvastam és elfogadom az <a target="_blank" href="' . getenv('TERMS') . '">Általános Szerződési Feltételeket</a>, valamint tudomásul veszem, hogy a megrendelés elküldése fizetési kötelezettséggel jár.'); ?></label>
												</div>
											<?php } ?>

											<!-- Captcha -->
											<div class="form-group">
												<label>Captcha</label>
												<span id="captcha-info" class="info"></span><br />
												<div class="input-group">
													<div class="input-group-prepend">
														<img id="captcha_code" src="<?php echo url('/captcha_code.php'); ?>" />
													</div>
													<input type="text" class="form-control captcha" name="captcha" id="captcha" required="">
													<div class="input-group-append">
														<button class="btn btn-secondary" type="button" id="captcha-refresh">
															<i class="fas fa-sync"></i>
														</button>
													</div>
												</div>
											</div>
											<!-- Captcha vége -->

											<input type="hidden" name="group_id" value="<?php echo (isset($groupId) && !is_null($groupId)) ? $groupId : ''; ?>">

											<div class="form-group">

												<button type="submit" class="btn btn-primary btn-lg btn-block "><?= getenv("BARION_PAYMENT") ?  Lang::instance()->_l('Tovább a fizetéshez') : Lang::instance()->_l('Küldés'); ?>
												</button>

												<?php if (getenv("BARION_PAYMENT")) { ?>
													<div class="barion_logo">
														<img src="/img/barion_300px.png" alt="barion" />
													</div>
												<?php } ?>
											<?php } ?>
											</div>
										</form>
									</div>
									<div class="col-lg-4">
										<div class="alert alert-danger" style="display: none;">
											<p><strong><?php echo Lang::instance()->_l('Hiba'); ?>:</strong></p>
											<ul id="error-msg"></ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- csomag feladás tab vége -->

				<!-- csomag napló lekérdezés tab -->
				<?php if ($tab === 'getpackage-tab' && getenv('GET_PACKAGEDATA_ON')) { ?>
					<?php $_SESSION['input_data'] = null; ?>
					<div id="pills-getpackage" class="container-fluid" style="max-width: 1400px;">
						<div class="tab-pane" id="get-packagedata-tab" role="tabpanel" aria-labelledby="get-packagedata-tab">
							<div class="card">
								<h1 class="card-header bg-primary text-white"><?php echo Lang::instance()->_l('Küldeményadatok lekérdezése'); ?></h1>
								<div class="card-body">
									<?php if (isset($message) && !is_null($message)) { ?>
										<div class="alert alert-success"><?php echo $message; ?></div>
									<?php } ?>
									<form method="POST" action="<?php echo url('/ajax.php?action=get'); ?>" id="get-form">
										<div class="row">

											<div class="col-lg-6">
												<div class="form-group">
													<label for="group_id">DeliveoID</label>
													<input name="group_id" type="text" value="<?php echo (isset($groupId) && !is_null($groupId)) ? $groupId : ''; ?>" class="form-control" id="group_id" required="">
												</div>
											</div>
											<div class="col-lg-4">
												<label>Captcha</label>
												<span id="captcha-info" class="info"></span><br />
												<div class="input-group">
													<div class="input-group-prepend">
														<img id="captcha_code" src="<?php echo url('/captcha_code.php'); ?>" />
													</div>
													<input type="text" class="form-control captcha" name="captcha" id="captcha" value="">
													<div class="input-group-append">
														<button class="btn btn-secondary" type="button" id="captcha-refresh">
															<i class="fas fa-sync"></i>
														</button>
													</div>
												</div>

											</div>
											<div class="col-lg-2">
												<button type="submit" class="btn btn-primary btn-block" style="margin-top: 32px;">
													OK</button>
											</div>
										</div>
									</form>
								</div>
							</div>

							<div class="card mt-3 hide" id="package-data">
								<h2 class="card-header bg-primary text-white "><?php echo Lang::instance()->_l('Csomag adatok'); ?></h2>
								<div class="card-body">
									<h2 class=" mt-2"><?php echo Lang::instance()->_l('Napló'); ?></h2>

									<ul id="package-log-result" class="list-group"></ul>

									<div class="row">
										<div class="col-lg-8 ">
											<?php if (getenv('TRACKING_SHOW_DATA') == null || getenv('TRACKING_SHOW_DATA') == 1) { ?>
												<h2 class=" mt-2"><?php echo Lang::instance()->_l('Feladó'); ?></h2>
												<ul id="sender-data" class="list-group"></ul>
												<h2 class=" mt-2"><?php echo Lang::instance()->_l('Címzett'); ?></h2>
												<ul id="consignee-data" class="list-group"></ul>
											<?php } ?>
											<h2 class=" mt-2"><?php echo Lang::instance()->_l('Tartalom'); ?></h2>
											<table class="table table-bordered" id="package-items">
												<thead>
													<tr>
														<th scope="col"><?php echo Lang::instance()->_l('Súly (kg)'); ?> </th>
														<th scope="col">X (cm)</th>
														<th scope="col">Y (cm)</th>
														<th scope="col">Z (cm)</th>
														<th scope="col"><?php echo Lang::instance()->_l('Egyedi azonosító'); ?> </th>
														<th scope="col"><?php echo Lang::instance()->_l('Cikkszám'); ?> </th>
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
										<div class="col-lg-4 mt-2">
											<div class="my-4" id="label">
												<a href="#" target="_blank" class="btn btn-outline-primary btn-block hide">
													<i class="fas fa-file-pdf"></i> <?php echo Lang::instance()->_l('Csomagcímke letöltése'); ?></a>
											</div>
											<?php if (getenv('DOWNLOAD_SIGNATURE_ENABLED')) { ?>
												<div class="my-4" id="signature">
													<a href="#" target="_blank" class="btn btn-outline-primary btn-block hide">
														<i class="fas fa-file-pdf"></i> <?php echo Lang::instance()->_l('Aláíráslap letöltése'); ?></a>
												</div>
											<?php } ?>
											<?php if (getenv('DISPLAY_TRACKINGURL')) { ?>
												<div class="my-4" id="trackingurl">
													<a href="#" target="_blank" class="hide">
														<i class="fas fa-map-marked-alt fa-3x"></i> </a>
												</div>
											<?php } ?>

											<div class="my-4" id="edit_group">
												<a href="#" class="btn btn-outline-primary btn-block edit_group hide">
													<i class="fas fa-edit"></i> <?php echo Lang::instance()->_l('Szerkesztés'); ?></a>
											</div>
											<div class="my-4" id="delete_group">
												<button type="button" data-group_id="" class="btn btn-outline-primary delete_group hide">
													<i class="fas fa-trash"></i> <?php echo Lang::instance()->_l('Törlés'); ?></button>
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				<?php } ?>
				<!-- csomag napló lekérdezés tab vége -->
		</div>
		<!-- tabok vége -->
	</div>

	<!-- assets -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="<?php echo url('/plugins/intl-tel-input-master/build/js/intlTelInput.js'); ?>"></script>
	<script>
		<?php if ($tab === 'send-tab' && getenv('SEND_ON')) { ?>
			class intlTel {
				constructor(parentSelector) {
					this.intlTelInputOptions = {
						// allowDropdown: false,
						// autoHideDialCode: false,
						// autoPlaceholder: "off",
						// dropdownContainer: document.body,
						// excludeCountries: ["us"],
						// formatOnDisplay: false,
						geoIpLookup: function(callback) {
							$.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
								var countryCode = (resp && resp.country) ? resp.country : "";
								callback(countryCode);
							});
						},
						// hiddenInput: "full_number",
						initialCountry: "auto",
						// localizedCountries: { 'de': 'Deutschland' },
						// nationalMode: false,
						//onlyCountries: ['hu', 'en'],
						// placeholderNumberType: "MOBILE",
						preferredCountries: ['hu'],
						// separateDialCode: true,
						utilsScript: "<?php echo url('/plugins/intl-tel-input-master/build/js/utils.js'); ?>",
					};
					this.errorMap = [
						"<?php echo Lang::instance()->_l('Érvénytelen szám'); ?>",
						"<?php echo Lang::instance()->_l('Érvénytelen ország kód'); ?>",
						"<?php echo Lang::instance()->_l('Túl rövid'); ?>",
						"<?php echo Lang::instance()->_l('Túl hosszú'); ?>",
						"<?php echo Lang::instance()->_l('Érvénytelen szám'); ?>"
					];
					this.input = document.querySelector(parentSelector + ' input[type=tel');
					this.errorMsg = document.querySelector(parentSelector + ' .error-msg');
					this.validMsg = document.querySelector(parentSelector + ' .valid-msg');
					this.iti = null;
				}

				init() {
					this.iti = window.intlTelInput(this.input, this.intlTelInputOptions);
				}

				reset() {
					this.input.classList.remove("error");
					this.errorMsg.innerHTML = "";
					this.errorMsg.classList.add("hide");
					this.validMsg.classList.add("hide");
				}

				setValidate() {
					this.input.addEventListener('blur', (event) => {
						this.reset();

						if (this.input.value.trim()) {
							if (this.iti.isValidNumber()) {
								this.validMsg.classList.remove("hide");
							} else {
								this.input.classList.add("error");
								var errorCode = this.iti.getValidationError();
								this.errorMsg.innerHTML = this.errorMap[errorCode];
								this.errorMsg.classList.remove("hide");
							}
						}
					});

					// on keyup / change flag: reset
					this.input.addEventListener('change', (event) => {
						this.reset();
					});
					this.input.addEventListener('keyup', (event) => {
						this.reset();
					});
				}
			}

			var intlTel1 = new intlTel('#sender_phone');
			intlTel1.init();
			intlTel1.setValidate();

			var intlTel2 = new intlTel('#consignee_phone');
			intlTel2.init();
			intlTel2.setValidate();

			var intlTel3 = new intlTel('#customer_phone');
			intlTel3.init();
			intlTel3.setValidate();
		<?php } ?>

		/*
		 * csomag sorok input tömb indexek újra rendezése
		 */
		var replaceInputArrayIndex = function(inputNameAttribute, newIndex) {
			var start = inputNameAttribute.indexOf('[');
			var end = inputNameAttribute.indexOf(']');

			var inputArrayIndex = inputNameAttribute.substring(start + 1, end);
			return inputNameAttribute.replace('[' + inputArrayIndex + ']', '[' + newIndex + ']');
		}
		var reOrderInputArrayIndex = function(selector) {
			$(selector).each((index, element) => {
				$(element).find('input, textarea').each((labelIndex, inputElemet) => {
					$(inputElemet).attr('name', this.replaceInputArrayIndex($(inputElemet).attr('name'), index));

				});
			});
		}

		function refreshCaptcha() {
			var d = new Date();
			$("#captcha_code").attr('src', 'captcha_code.php?v=' + d.getTime());
		}

		function getQuote() {
			$.ajax({
				method: "POST",
				url: "<?php echo url('/ajax.php?action=getQuote'); ?>",
				data: $('#send-form').serialize(),
				dataType: "json"
			}).done(function(response) {
				if (response.type == "success") {

					if (!response.data.quote) {
						response.data.quote = 0;
					}

					$(".service_price h4").html(response.data.quote + " " + response.data.currency);
					$(".service_price .price_to_pay").val(response.data.quote);
					$(".service_price .currency").val(response.data.currency);
				} else {
					$(".service_price h4").html('???');
					$(".service_price .price_to_pay").val(0);
					$(".service_price .currency").val("HUF");
				}
			});
		}

		$(document).ready(function() {
			var senderInputs = ['sender', 'sender_zip', 'sender_city', 'sender_address', 'sender_apartment', 'sender_email', 'sender_phone'];
			var consigneeInputs = ['consignee', 'consignee_zip', 'consignee_city', 'consignee_address', 'consignee_apartment', 'consignee_email', 'consignee_phone'];
			var customerInputs = ['customer', 'customer_zip', 'customer_city', 'customer_address', 'customer_apartment', 'customer_email', 'customer_phone'];

			<?php if (isset($input_data)) { ?>
				// sender_country,
				$.each(senderInputs, function(index, name) {
					$('input[name="' + customerInputs[index] + '"]').val($('input[name="' + name + '"]').val());
				});

				$('select[name="customer_country"]').val($('select[name="sender_country"] option:selected').val());

				getQuote();

			<?php } else { ?>

				// var userLang = navigator.language || navigator.userLanguage;

				$('select[name="sender_country"]').val('<?php echo $default_country; ?>');
				$('select[name="consignee_country"]').val('<?php echo $default_country; ?>');
				$('select[name="customer_country"]').val('<?php echo $default_country; ?>');

			<?php } ?>

			<?php
			//ha van beállítva megbízó azonosító 
			if (getenv('CUSTOMER_ID')) {
			?>
				$.ajax({
					method: "POST",
					url: "<?php echo url('/ajax.php?action=getCustomerData'); ?>",
					data: {
						customer_id: '<?php echo getenv('CUSTOMER_ID'); ?>'
					},
				}).done(function(resp) {
					var o = JSON.parse(resp);

					if (typeof o.customer_data.data[0] !== 'undefined') {
						$('input[name=customer_id]').val(o.customer_data.data[0].customer_id);

						$('input[name=customer_zip]').prop('required', false);
						$('input[name=customer_city]').prop('required', false);
						$('input[name=customer_address]').prop('required', false);
						$('input[name=customer_apartment]').prop('required', false);
						$('select[name=customer_country]').prop('required', false);
						$('input[name=customer_email]').prop('required', false);
						$('input[name=customer_phone]').prop('required', false);
						$('.customer_container').hide();
					}
				});
			<?php } ?>

			$('input:radio[name="customer_data"]').on('change', function() {
				var selected = $(this).val();

				if (selected == 1) {
					// sender_country,
					$.each(senderInputs, function(index, name) {
						$('input[name="' + customerInputs[index] + '"]').val($('input[name="' + name + '"]').val());
					});

					$('select[name="customer_country"]').val($('select[name="sender_country"] option:selected').val());
				} else if (selected == 2) {
					$.each(consigneeInputs, function(index, name) {
						$('input[name="' + customerInputs[index] + '"]').val($('input[name="' + name + '"]').val());
					});
					$('select[name="customer_country"]').val($('select[name="consignee_country"] option:selected').val());

				} else if (selected == 0) {

					$.each(customerInputs, function(index, name) {
						$('input[name="' + name + '"]').val('');
					});
					$('select[name="customer_country"]').val('hu');
				}
			});

			$('input[name^="sender"]:not(input[name="sender"])').on('keyup', function() {
				if ($('input:radio[name="customer_data"]:checked').val() == 1) {
					var inputName = $(this).attr('name');
					var customerInputName = inputName.replace("sender", "customer");
					$('input[name="' + customerInputName + '"]').val($(this).val());
				}
			});
			$('input[name="sender"]').on('keyup', function() {
				if ($('input:radio[name="customer_data"]:checked').val() == 1) {
					$('input[name="customer"]').val($('input[name="sender"]').val());
				}
			});
			$('input[name^="consignee"]:not(input[name="consignee"])').on('keyup', function() {
				if ($('input:radio[name="customer_data"]:checked').val() == 2) {
					var inputName = $(this).attr('name');
					var customerInputName = inputName.replace("consignee", "customer");
					$('input[name="' + customerInputName + '"]').val($(this).val());
				}
			});
			$('input[name="consignee"]').on('keyup', function() {
				if ($('input:radio[name="customer_data"]:checked').val() == 2) {
					$('input[name="customer"]').val($('input[name="consignee"]').val());
				}
			});

			$('.delete_group').click(function() {

				if (confirm('<?php echo getenv('MSG_CONFIRM_DELETE'); ?>')) {
					$.ajax({
						method: "POST",
						url: "<?php echo url('/ajax.php?action=deleteGroup'); ?>",
						data: {
							group_id: $(this).attr('data-group_id')
						},
					}).done(function(resp) {
						window.location.replace('/?tab=send-tab');
					});
				}

			});

			// Csomaghoz tartozó módosítások status értékeihez rendelt szövegek és ikonok (lásd api dokumentáció: 9. Napló lekérése)
			var status = {
				'rogzitve': {
					'text': 'Rögzítve',
					'icon': 'far fa-check-square fa-2x text-muted'
				},
				'feladva': {
					'text': 'Feladva',
					'icon': 'fas fa-cubes fa-2x text-info'
				},
				'rendszamhoz_rendel': {
					'text': 'Redszámhoz rendelt',
					'icon': 'fas fa-truck-loading fa-2x text-info'
				},
				'futar_felvette': {
					'text': 'Futár felvette',
					'icon': 'fas fa-truck fa-2x text-primary'
				},
				'sikeres': {
					'text': 'Sikeres kézbesítés',
					'icon': 'fas fa-people-carry fa-2x text-success'
				},
				'kezbesitesi_kiserlet': {
					'text': 'Kézbesítési kisérlet',
					'icon': 'fas fa-exclamation-triangle fa-2x text-warning'
				}
			};
			// csomag sor hozzáadás
			$('#add-package-row').on('click', (event) => {
				var packageInputRow = $('#packages tbody tr:first').clone();
				$(packageInputRow).find('input').val('');
				let newRow = $(packageInputRow);
				newRow.appendTo("#packages tbody");
				reOrderInputArrayIndex('#packages tbody tr');
			});
			// csomag sor törlés
			$(document).on('click', '#packages button.remove-item', (event) => {
				if ($('#packages tbody tr').length > 1) {
					$(event.currentTarget).closest('tr').remove();
					calculateSummary();
					reOrderInputArrayIndex('#packages tbody tr');
				}
			});
			// csomag szám paraméterek 0-nál nagyobbak ellenőrzés
			$(document).on('blur', '#packages input[type=number]', (event) => {
				if ($(event.currentTarget).val() <= 0) {
					$(event.currentTarget).val('');
				}
			});

			$('#captcha-refresh').on('click', (event) => {
				event.preventDefault();
				refreshCaptcha();
			});
			// csomag feladás űrlap küldése
			$('#send-form').on('submit', (event) => {
				event.preventDefault();
				// captcha ellenőrzés küldés elött
				$.ajax({
					method: "POST",
					url: "<?php echo url('/ajax.php?action=checkcaptcha'); ?>",
					data: {
						captcha: $('#captcha').val()
					},
					dataType: "json"
				}).done(function(response) {

					if ('success' in response) {
						var confirmed = confirm('<?php echo getenv('MSG_CONFIRM_SEND'); ?>');
						if (confirmed) {
							$.ajax({
								method: "POST",
								url: $(event.currentTarget).attr('action'),
								data: $(event.currentTarget).serialize(),
								dataType: "json",
								beforeSend: function() {
									$('#send-form :submit').prop('disabled', true).append(' <i class="fas fa-sync fa-spin"></i> ');
								}
							}).done(function(data) {

								$('#send-form :submit').prop('disabled', false);
								$('#send-form :submit').find('i').remove();

								if ('errors' in data) {
									$('#error-msg').html('');
									$('.alert.alert-danger').show();
									$.each(data.errors, function(index, message) {
										$('#error-msg').append('<li>' + message + '</li>');
									});
								} else if (data.error_code == 0 && data.type == 'success') {
									var group_id = $('input[name=group_id]').val();

									if (group_id) {
										$('#send-form :input').val('');
										$('#error-msg').html('');
										$('.alert.alert-danger').hide();
										if (typeof data.data !== 'undefined') {
											$('.alert.alert-success').show();
											$('.alert.alert-success').html('<p>' + data.msg + '</p>');
										}
										window.location.replace('/?tab=getpackage-tab&groupId=' + group_id);

									} else {
										$('#send-form :input').val('');
										$('#error-msg').html('');
										$('.alert.alert-danger').hide();
										if (typeof data.data !== 'undefined') {
											$('.alert.alert-success').show();
											$('.alert.alert-success').html('<p>' + data.msg + '</p> <p>' + data.data[0] + '</p>');
										}

										<?php if (getenv('BARION_PAYMENT')) { ?>
											if (data.type == "success") {
												window.location.replace(data.barion_url);
											} else {
												alert(data.msg);
											}
										<?php } else { ?>
											window.location.replace('/?tab=getpackage-tab&groupId=' + data.data[0] + '&msg=' + data.msg);
										<?php } ?>
									}

								} else if (data.error_code == 4012) {
									alert(data.msg + ': ' + data.field);
								} else if (data.error_code > 0) {
									alert(data.msg);
								} else if (data.error_msg) {
									alert(data.error_msg);
								}
							});
						}

					} else {
						alert(response.error);
					}
				});
			});

			var calculateSummary = function() {
				//tömeg számítása
				var total_weight = 0;
				$($('.packages_weight')).each(function() {
					if (parseInt($(this).val()) > 0) {
						total_weight += parseInt($(this).val());
					}
				});

				$('.weight_summary').html(total_weight);

				//térfogat számítása
				var total_volume = 0;
				var total_dim = 0;

				$($('.packages_x')).each(function(index_x) {
					if (parseInt($(this).val()) > 0) {

						var x = parseInt($(this).val());
						var y = 0;
						$($('.packages_y')).each(function(index_y) {
							if (index_x == index_y) {
								if (parseInt($(this).val()) > 0) {
									y = parseInt($(this).val());
								}
							}
						});
						var z = 0;
						$($('.packages_z')).each(function(index_z) {
							if (index_x == index_z) {
								if (parseInt($(this).val()) > 0) {
									z = parseInt($(this).val());
								}
							}
						});
						var current_volume = (x * y * z);
						total_volume += current_volume;
						total_dim += (current_volume / 5000);
					}
				});

				$('.volume_summary').html(total_volume);

				$('.dim_summary').html(total_dim.toFixed(2));
			}


			var getPackageData = function() {

				$.ajax({
					method: "POST",
					url: $('#get-form').attr('action'),
					data: $('#get-form').serialize(),
					dataType: "json",
					beforeSend: function() {
						$('#get-form :submit').append(' <i class="fas fa-sync fa-spin"></i> ');
					}
				}).done(function(response) {
					$('#get-form :submit i').remove();
					$('#package-data').show();
					$('#package-data .card-header').html($('#get-form input[name="group_id"]').val());

					if (response.packageLog.error_code == 0 && response.packageLog.type == 'success') {

						if (response.packageLog.data.length == 0) {
							alert('<?php echo getenv('MSG_PACKAGE_NOT_FOUND'); ?>');
						} else {

							$('#package-data .card-header').html($('#get-form input[name="group_id"]').val());

							$.each(response.packageLog.data, function(index, item) {
								var date = new Date(item.timestamp * 1000);
								var date = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + (date.getDate())).slice(-2) +
									' ' + ('0' + (date.getHours())).slice(-2) + ':' + ('0' + (date.getMinutes())).slice(-2);

								var itemView = '<li class="list-group-item"><div class="row">';

								if (item.status in status) {
									itemView += '<div class="col-1"><i class="' + status[item.status].icon + ' mr-2"></i> </div>';
									itemView += '<div class="col-2">' + status[item.status].text + '</div>';
								} else {
									itemView += '<div class="col-3">' + item.status + ' </div>';
								}

								itemView += '<div class="col-3">' + item.status_text + '</div>';
								itemView += '<div class="col-3">' + date + '</div>';
								itemView += '<div class="col-3">' + item.user + '</div>';
								itemView += '</div></li>';

								$('#package-log-result').append(itemView);
							});

							if ($('#signature').length) {

								$('#signature a').show().attr('href', '/signature.php?groupId=' + $('#get-form input[name="group_id"]').val());
							}
							if ($('#label').length) {
								$('#label a').show().attr('href', '/label.php?groupId=' + $('#get-form input[name="group_id"]').val());
							}

							//ha a csomag még nincs fizetve vagy ingyenes volt és a futár még nem vette fel, akkor a szerkesztés gombot megjeleníthetjük
							<?php if (getenv('EDIT_DELETE_PACKAGE') == 1) { ?>
								if (!response.getPackage.data[0].picked_up && !response.getPackage.data[0].dropped_off && !response.getPackage.data[0].paid) {
									$('.edit_group').show().attr('href', '/?tab=send-tab&groupId=' + $('#get-form input[name="group_id"]').val());
									$('.delete_group').show().attr('data-group_id', $('#get-form input[name="group_id"]').val());
								}
							<?php } ?>

							if ($('#trackingurl').length) {
								$('#trackingurl a').show().attr('href', response.getPackage.data[0].trackingurl);
							}
						}
					}

					if (response.getPackage.error_code == 0 && response.getPackage.type == 'success') {

						var packageData = response.getPackage.data[0];
						<?php if (getenv('TRACKING_SHOW_DATA') == null || getenv('TRACKING_SHOW_DATA') == 1) { ?>
							$('#sender-data, #consignee-data').html('');
							var view = '';
							view += '<li class="list-group-item">' + packageData.sender + '</li>';
							view += '<li class="list-group-item">' + packageData.sender_zip + ' ';
							view += '' + packageData.sender_city + ', ';
							view += '' + packageData.sender_address + ' ';
							view += '' + packageData.sender_apartment + '</li>';
							view += '<li class="list-group-item">' + packageData.sender_phone + '</li>';
							view += '<li class="list-group-item">' + packageData.sender_email + '</li>';
							$('#sender-data').append(view);

							view = '';
							view += '<li class="list-group-item">' + packageData.consignee + '</li>';
							view += '<li class="list-group-item">' + packageData.consignee_zip + ' ';
							view += '' + packageData.consignee_city + ', ';
							view += '' + packageData.consignee_address + ' ';
							view += '' + packageData.consignee_apartment + '</li>';
							view += '<li class="list-group-item">' + packageData.consignee_phone + '</li>';
							view += '<li class="list-group-item">' + packageData.consignee_email + '</li>';
							$('#consignee-data').append(view);
						<?php } ?>

						var packageItemsTableRows = '';
						$.each(packageData.packages, function(index, packageItem) {
							packageItemsTableRows += '<tr>';
							packageItemsTableRows += '<td>' + packageItem.weight + '</td>';
							packageItemsTableRows += '<td>' + packageItem.x + '</td>';
							packageItemsTableRows += '<td>' + packageItem.y + '</td>';
							packageItemsTableRows += '<td>' + packageItem.z + '</td>';
							packageItemsTableRows += '<td>' + packageItem.customcode + '</td>';
							packageItemsTableRows += '<td>' + (packageItem.item_no != null ? packageItem.item_no : '') + '</td>';
							packageItemsTableRows += '</tr>';
						});
						$('#package-items tbody').html(packageItemsTableRows);
					}
				});
			}

			if ($('input[name="group_id"').length && $('input[name="group_id"').val() != '') {
				getPackageData();
			}

			// csomag napló lekérdezés - űrlap küldés
			$('#get-form').on('submit', function(event) {
				event.preventDefault();
				$('#package-log-result').html('<li><i class="fas fa-spinner fa-spin fa-5x"></i></li>');

				$.ajax({
					method: "POST",
					url: "<?php echo url('/ajax.php?action=checkcaptcha'); ?>",
					data: {
						captcha: $('#captcha').val()
					},
					dataType: "json"
				}).done(function(response) {
					$('#package-log-result').html('');
					if ('error' in response) {
						alert(response.error);
					} else {
						getPackageData();
					}
				});

			});

			var priceCalcInputs = ['sender_zip', 'sender_city', 'sender_address', 'consignee_zip', 'consignee_city', 'consignee_address', 'customer_zip', 'customer_city', 'customer_address'];

			var selector_string_helper = [];

			$.each(priceCalcInputs, function(index, name) {
				selector_string_helper.push('input[name="' + name + '"]');
			});

			selector_string_helper.push('select[name="customer_country"]');
			selector_string_helper.push('select[name="delivery"]');
			selector_string_helper.push('input[name="optional_parameter_2"]');
			selector_string_helper.push('input[name="referenceid"]');
			selector_string_helper.push('input[name="tracking"]');
			selector_string_helper.push('input[name="customer_data"]');
			selector_string_helper.push('input[name="cod"]');
			selector_string_helper.push('input[name="optional_parameter_1"]');
			selector_string_helper.push('input[name="optional_parameter_3"]');

			var selector_string = selector_string_helper.join(',');

			//szolgáltalás díjának lekérdezése
			$(selector_string).change(function() {
				getQuote();
			});

			$("#add-package-row, .remove-item").click(function() {
				getQuote();
			});

			//tömeg számítása
			$('body').on('keyup', '.packages_weight', function() {
				calculateSummary();
				getQuote();
			});
			$('body').on('change', '.packages_weight', function() {
				calculateSummary();
				getQuote();
			});

			//térfogat számítása
			$('body').on('keyup', '.packages_x, .packages_y, .packages_z', function() {
				calculateSummary();
				getQuote();
			});

			$('body').on('change', '.packages_x, .packages_y, .packages_z', function() {
				calculateSummary();
				getQuote();
			});

			//szerkesztéshez a csomagcsoport adatainak betöltése
			<?php if ($tab == 'send-tab' && $groupId) { ?>
				$.ajax({
					method: "POST",
					url: "<?php echo url('/ajax.php?action=getEditData'); ?>",
					data: {
						group_id: '<?php echo $groupId; ?>'
					},
				}).done(function(data) {
					var obj = JSON.parse(data);

					$('input[name=group_id]').val('<?php echo $groupId; ?>');

					$('input[name=sender]').val(obj.getPackage.data[0].sender);
					$('select[name=sender_country]').val(obj.getPackage.data[0].sender_country.toLowerCase()).change();

					$('input[name=sender_zip]').val(obj.getPackage.data[0].sender_zip);
					$('input[name=sender_city]').val(obj.getPackage.data[0].sender_city);
					$('input[name=sender_address]').val(obj.getPackage.data[0].sender_address);
					$('input[name=sender_apartment]').val(obj.getPackage.data[0].sender_apartment);
					$('input[name=sender_phone]').val(obj.getPackage.data[0].sender_phone);
					$('input[name=sender_email]').val(obj.getPackage.data[0].sender_email);

					$('input[name=consignee]').val(obj.getPackage.data[0].consignee);
					$('select[name=consignee_country]').val(obj.getPackage.data[0].consignee_country.toLowerCase()).change();
					$('input[name=consignee_zip]').val(obj.getPackage.data[0].consignee_zip);
					$('input[name=consignee_city]').val(obj.getPackage.data[0].consignee_city);
					$('input[name=consignee_address]').val(obj.getPackage.data[0].consignee_address);
					$('input[name=consignee_apartment]').val(obj.getPackage.data[0].consignee_apartment);
					$('input[name=consignee_phone]').val(obj.getPackage.data[0].consignee_phone);
					$('input[name=consignee_email]').val(obj.getPackage.data[0].consignee_email);

					$('select[name=delivery]').val(obj.getPackage.data[0].delivery_id);
					$('select[name=group_currency]').val(obj.getPackage.data[0].currency);
					$('input[name=optional_parameter_1]').val(obj.getPackage.data[0].optional_parameter_1);
					$('input[name=referenceid]').val(obj.getPackage.data[0].referenceid);
					$('input[name=tracking]').val(obj.getPackage.data[0].tracking);
					$('input[name=cod]').val(obj.getPackage.data[0].cod);
					$('textarea[name=comment]').val(obj.getPackage.data[0].comment);

					$('input[name=customer_data][value=0]').prop('checked', true);

					if (obj.getPackage.data[0].customer_id) {
						$('input[name=customer_id]').val(obj.getPackage.data[0].customer_id);
						$('input[name=customer]').val(obj.getPackage.data[0].customer_name);
						$('input[name=customer]').attr('readonly', 'true');

						$.ajax({
							method: "POST",
							url: "<?php echo url('/ajax.php?action=getCustomerData'); ?>",
							data: {
								customer_id: obj.getPackage.data[0].customer_id
							},
						}).done(function(resp) {
							var o = JSON.parse(resp);

							$('input[name=customer_zip]').val(o.customer_data.data[0].customer_zip);
							$('input[name=customer_city]').val(o.customer_data.data[0].customer_city);
							$('input[name=customer_address]').val(o.customer_data.data[0].customer_address);
							$('input[name=customer_apartment]').val(o.customer_data.data[0].customer_building);
							$('select[name=customer_country]').val(o.customer_data.data[0].customer_country.toLowerCase()).change();
							$('input[name=customer_email]').val(o.customer_data.data[0].customer_email);
							$('input[name=customer_phone]').val(o.customer_data.data[0].customer_phone);

							//a csomag módosításánál apin nincs lehetőség arra, hogy az ügyfelet is módosítsuk
							$('input[name=customer_zip]').attr('readonly', true);
							$('input[name=customer_city]').attr('readonly', true);
							$('input[name=customer_address]').attr('readonly', true);
							$('input[name=customer_apartment]').attr('readonly', true);
							$('select[name=customer_country]').attr('readonly', true);
							$('input[name=customer_email]').attr('readonly', true);
							$('input[name=customer_phone]').attr('readonly', true);
						});
					}

					if (obj.getPackage.data[0].optional_parameter_1) {
						$('input[name=optional_parameter_1]').prop('checked', true);
					}
					if (obj.getPackage.data[0].optional_parameter_3) {
						$('input[name=optional_parameter_3]').prop('checked', true);
					}

					$(obj.getPackage.data[0].packages).each(function(index, package) {
						$('.packages_weight').eq(index).val(package.weight);
						$('.packages_x').eq(index).val(package.x);
						$('.packages_y').eq(index).val(package.y);
						$('.packages_z').eq(index).val(package.z);
						$('.packages_custom_code').eq(index).val(package.customcode);
						$('.packages_package_id').eq(index).val(package.package_id);

						$('.packages_weight').eq(index).trigger('change');
					});

					$('#send-form').attr('action', '<?php echo url("/ajax.php?action=edit"); ?>');
				});
			<?php } ?>

		});
	</script>


</body>

</html>