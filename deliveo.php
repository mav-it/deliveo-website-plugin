<?php

/**
 * Deliveo API réteg
 *
 * @author Jónás Zsolt
 */
class Deliveo
{

    /**
     * GuzzleHttp client
     * 
     * @var object 
     */
    protected $client, $langCode;


    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client(
            ["headers" => [
                "Source" => "DWP",

            ]]
        );
        $langCodes = Lang::instance()->getAcceptLang();
        $this->langCode = in_array($_SESSION['lang'], $langCodes) ? $_SESSION['lang'] : 'en_GB';
    }

    /**
     * A kötelező mezők lekérdezésével ellenőrzi a kulcs érvényességét.
     * 
     * @return boolean
     */
    public function validKey()
    {
        $res = $this->client->request('GET', $this->url('test'));
        $response = json_decode($res->getBody());

        $result['is_valid'] = $response->data->valid;
        $result['default_currency'] = $response->data->default_currency;
        $result['default_country'] = strtolower($response->data->default_country);

        return $result;
    }

    /**
     * Szállítási opciók lekérése.
     * 
     * * @return array
     */
    public function getDelivery()
    {
        $res = $this->client->request('GET', $this->url('delivery'));

        if ($res->getStatusCode() === 200) {
            $response = json_decode($res->getBody());
            $options = [];
            foreach ($response->data as $row) {
                $options[$row->value] = $row->description;
            }
            return $options;
        } else {
            die("StatusCode: " . $res->getStatusCode());
        }
    }

    /**
     * Lokációk lekérdezése.
     * 
     * @return array
     */
    public function getLocations($onlyPickupPoints = false)
    {
        $res = $this->client->request('GET', $this->url('locations'));
        if ($res->getStatusCode() === 200) {
            $response = json_decode($res->getBody(), true);
            if ($onlyPickupPoints) {
                $response['data'] = array_filter($response['data'], function ($location) {
                    return $location['type'] == 2 && $location['active'] == 1;
                });
                $response['data'] = array_values($response['data']);
            }
            return $response['data'];
        } else {
            die("StatusCode: " . $res->getStatusCode());
        }
    }

    /**
     * Csomag feladása.
     * 
     * @param array $postData
     * @return object
     */
    public function packageCreate($postData)
    {
        $res = $this->client->request('POST', $this->url('package/create'), ['form_params' => $postData]);
        if ($res->getStatusCode() === 200) {
            $response = json_decode($res->getBody(), true);
            if ($response['type'] != "error" && $response['data'] && $postData['pickup_location_id']) {
                $this->assignTo($response['data'][0], $postData['pickup_location_id']);
            }
            return json_decode($res->getBody());
        } else {
            die("StatusCode: " . $res->getStatusCode());
        }
    }

    /**
     * Küldemény hozzárendelése járműhöz/csomagponthoz.
     * 
     * @param array $postData
     * @return object
     */
    public function assignTo($deliveoID, $locationID)
    {
        $res = $this->client->request('POST', $this->url('package/' . $deliveoID . '/assignto'), ['form_params' => [
            'picked_up_location_id' => $locationID
        ]]);

        if ($res->getStatusCode() === 200) {
            return true;
        } else {
            die("StatusCode: " . $res->getStatusCode());
        }
    }

    /**
     * Csomag törlése.
     * 
     * @param array $postData
     * @return object
     */
    public function deleteGroup($group_id)
    {
        $res = $this->client->request('POST', $this->url('package/' . $group_id . '/delete'));
        if ($res->getStatusCode() === 200) {
            return json_decode($res->getBody());
        } else {
            die("StatusCode: " . $res->getStatusCode());
        }
    }


    /**
     * Csomag szerkesztése.
     * 
     * @param array $postData
     * @return object
     */
    public function packageEdit($postData)
    {
        $res = $this->client->request('POST', $this->url('package/' . $postData['group_id'] . '/edit'), ['form_params' => $postData]);
        if ($res->getStatusCode() === 200) {
            return json_decode($res->getBody());
        } else {
            die("StatusCode: " . $res->getStatusCode());
        }
    }


    /**
     * Csomag adatainak lekérése.
     * 
     * @param string $groupId Csoportkód
     * @return object
     */
    public function getPackage($groupId)
    {
        $res = $this->client->request('GET', $this->url('package/' . $groupId));
        if ($res->getStatusCode() === 200) {
            return json_decode($res->getBody());
        } else {
            die("StatusCode: " . $res->getStatusCode());
        }
    }


    public function getCustomer($customerId)
    {
        $res = $this->client->request('GET', $this->url('customer/' . $customerId));
        if ($res->getStatusCode() === 200) {
            return json_decode($res->getBody());
        } else {
            die("StatusCode: " . $res->getStatusCode());
        }
    }


    /**
     * Napló lekérése.
     * 
     * Csomaghoz tartozó módosítások lekérése.
     * 
     * @param string $groupId Csoportkód
     * @return object
     */
    public function packageLog($groupId)
    {
        $res = $this->client->request('GET', $this->url('package_log/' . $groupId));

        if ($res->getStatusCode() === 200) {
            return json_decode($res->getBody());
        } else {
            die("StatusCode: " . $res->getStatusCode());
        }
    }

    /**
     * Ügyfél létrehozása
     * 
     * @param array $postData
     * @return object
     */
    public function customerCreate($postData)
    {
        $res = $this->client->request('POST', $this->url('customer/create'), ['form_params' => $postData]);
        if ($res->getStatusCode() === 200) {
            return json_decode($res->getBody());
        } else {
            die("StatusCode: " . $res->getStatusCode());
        }
    }

    /**
     * Csomagcímke url
     * 
     * @param string $groupId Csoportkód
     * @return object
     */
    public function labelUrl($groupId)
    {
        return $this->url('label/' . $groupId);
    }

    /**
     * Aláíráslap url
     * 
     * @param string $groupId Csoportkód
     * @return type
     */
    public function signatureUrl($groupId)
    {
        $res = $this->client->request('GET', $this->url('signature/' . $groupId));

        if ($res->getStatusCode() === 200) {
            $response = json_decode($res->getBody());


            if (is_object($response) && $response->type === 'error') {
                return false;
            } else {
                return $this->url('signature/' . $groupId);
            }
        } else {
            die("StatusCode: " . $res->getStatusCode());
        }
    }

    /**
     * Az api kérésekhez szükséges url-t állítja össze.
     * 
     * Pl.: http://deliveo.nanosoft.hu/api/package/v2/[PARANCS]?licence=LICENCE&api_key=API_KEY&lang=hu_HU
     * 
     * @param string $command PARANCS
     * @param array $parameters további opcionális paraméterek
     * @return string
     */
    private function url($command, $parameters = null)
    {

        $url = getenv('API_URL') . $command . '?licence=' . getenv('LICENCE') . '&api_key=' . getenv('API_KEY') . '&lang=' . $this->langCode;

        if (!is_null($parameters) && is_array($parameters)) {
            foreach ($parameters as $parameterKey => $parameterValue) {
                $url .= '&' . $parameterKey . '=' . $parameterValue;
            }
        }

        return $url;
    }
}
