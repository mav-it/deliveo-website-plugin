<?php

final class Lang
{

    private $trans = null;
    private $acceptLang = [
        'en_GB',
        'hu_HU',
        'de_DE',
        'el_GR',
        'fr_FR',
        'es_ES',
        'cs_CZ',
        'pl_PL',
        'ro_RO',
        'it_IT',
        'bg_BG',
        'si_SL',
        'nb_NO',
        'tr_TR',
        'da_DK',
        'et_EE',
        'fi_FI',
        'lt_LT',
        'lv_LV',
        'pt_PT',
        'sk_SK',
        'sv_SE',
    ];

    //nyelvi kódok és szövegek adott nyelvet
    private $langTexts = [
        'hu_HU' => 'Magyar',
        'fr_FR' => 'Français',
        'es_ES' => 'Español',
        'cs_CZ' => 'Čeština',
        'de_DE' => 'Deutsch',
        'el_GR' => 'Ελληνικά',
        'pl_PL' => 'Polski',
        'ro_RO' => 'Română',
        'it_IT' => 'Italiano',
        'bg_BG' => 'Български',
        'si_SL' => 'Slovenščina',
        'nb_NO' => 'Norsk',
        'tr_TR' => 'Türkçe',
        'en_GB' => 'English',
        'da_DK' => 'Dansk',
        'et_EE' => 'Eestonian',
        'fi_FI' => 'Suomalinen',
        'lt_LT' => 'Lietuvos',
        'lv_LV' => 'Latvijas',
        'pt_PT' => 'Português',
        'sk_SK' => 'Slovenský',
        'sv_SE' => 'Svenska',
    ];

    /**
     * Call this method to get singleton
     *
     * @return Lang
     */
    public static function instance()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new Lang();
        }
        return $inst;
    }

    /**
     * Private v so nobody else can instantiate it
     *
     */
    private function __construct()
    {
        if (!isset($_SESSION['lang'])) {
            $lang = str_replace('-', '_', substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 5));
            if ($lang == "en") {
                $lang = "en_GB";
            }

            $_SESSION['lang'] = in_array($lang, $this->acceptLang) ? $lang : 'en_GB';
        } else {
        }

        if (file_exists(__DIR__ . '/' . filter_var($_SESSION['lang']) . '.json')) {
            $this->trans = json_decode(file_get_contents(__DIR__ . '/' . filter_var($_SESSION['lang']) . '.json'), true);
        }
    }

    public function _l($text)
    {
        if ($this->trans && array_key_exists($text, $this->trans) && $this->trans[$text] != '') {
            return $this->trans[$text];
        } else {
            return $text;
        }
    }

    public function getAcceptLang()
    {
        return $this->acceptLang;
    }

    public function getdeliveoApiLangCodes()
    {
        return $this->deliveoApiLangCodes;
    }
    public function getlangText($langcode)
    {
        return (array_key_exists($langcode,  $this->langTexts)) ? $this->langTexts[$langcode] : '';
    }
}
